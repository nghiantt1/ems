Admin.create! email: 'admin@gmail.com',
              password: '123456',
              password_confirmation: '123456'

User.create! name: 'User',
             email: 'user@gmail.com',
             password: '123456',
             password_confirmation: '123456',
             role: 0

10.times do
  Blog.create! title: FFaker::Lorem.sentence(10),
               category: Blog.categories.keys.sample,
               public_time: Time.zone.now,
               is_public: true,
               is_recommend: true,
               image: File.open(Dir['app/assets/images/*.jpg'].sample),
               body: FFaker::Lorem.paragraph(5),
               author_picture: File.open('app/assets/images/author-image.png'),
               token: Blog.new_token
end

10.times do
  Blog.create! title: FFaker::Lorem.sentence(10),
               category: Blog.categories.keys.sample,
               public_time: Time.zone.now + 10.minutes,
               is_public: true,
               image: File.open(Dir['app/assets/images/*.jpg'].sample),
               body: FFaker::Lorem.paragraph(5),
               author_picture: File.open('app/assets/images/author-image.png'),
               token: Blog.new_token
end

5.times do
  Blog.create! title: FFaker::Lorem.sentence(10),
               category: Blog.categories.keys.sample,
               public_time: Time.zone.now,
               image: File.open(Dir['app/assets/images/*.jpg'].sample),
               body: FFaker::Lorem.paragraph,
               author_picture: File.open('app/assets/images/author-image.png'),
               token: Blog.new_token
end
