class React < ApplicationRecord
  belongs_to :blog
  belongs_to :user

  ATTR_PARAMS = [
    :user_id,
    :blog_id,
    :emotion,
    :code
  ].freeze

  enum emotion: %w[love like sad sorrow]
  scope :count_vote, ->(emotion) { where 'emotion = ?', emotion }

  validates :blog_id, presence: true
  validates :emotion, presence: true

  class << self
    def new_token
      chars = (0..9).to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      chars.sample(16).join
    end
  end
end
