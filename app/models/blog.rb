class Blog < ApplicationRecord
  has_many :reacts, dependent: :destroy
  enum category: %w[family school study]

  mount_uploader :image, ImageUploader
  mount_uploader :author_picture, AuthorPictureUploader

  ATTR_PARAMS = [
    :title,
    :category,
    :public_time,
    :is_public,
    :is_recommend,
    :image,
    :body,
    :author_name,
    :author_position,
    :author_age,
    :author_picture
  ].freeze

  validates :title, presence: true, length: { maximum: 255 }
  validates :category, presence: true
  validates :public_time, presence: true
  validates :body, presence: true
  validates :image, presence: true
  validates :author_name, presence: true
  validates :author_position, presence: true
  validates :author_age, presence: true
  validates :author_picture, presence: true

  validate :validate_image_size
  validate :validate_author_age

  scope :sort_blog, ->(param_sort) { order public_time: param_sort }
  scope :public_blogs, lambda {
    where 'is_public = ? AND public_time < ?', true, Time.zone.now
  }
  scope :recommend_blogs, -> { where 'is_recommend = ?', true }
  scope :blog_by_category, ->(category) { where 'category = ?', category }

  class << self
    def new_token
      chars = (0..9).to_a + ('a'..'z').to_a + ('A'..'Z').to_a
      chars.sample(16).join
    end
  end

  private

  def validate_image_size
    return unless image.size > Settings.image.max_capacity.megabytes
    errors.add :image,
               I18n.t('error_capacity_image',
                      maximum: Settings.image.max_capacity)
  end

  def validate_author_age
    return errors.add I18n.t('error_age') if author_age < 0
  end
end
