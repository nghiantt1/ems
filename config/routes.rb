Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admins, path: 'admins'
  devise_for :users, path: 'users', controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  root 'blogs#index'

  namespace :admin do
    resources :blogs
    get '/', to: 'blogs#home'
  end

  resources :blogs, only: [:show, :index]
  resources :users, only: [:show, :update, :edit]
  resources :reacts, only: :create
  delete 'reacts', to: 'reacts#destroy'
  patch 'reacts', to: 'reacts#update'
  get 'messages/index'
  get 'blogs/:id/:token', to: 'blogs#show', as: 'token_blog'
  get 'app_author', to: 'blogs#app_author'
end
