$(document).ready(function(){
  $('.login-btn').on('click', function(){
    $("#modal-login").modal();
  });

  $('#sign-in').on('ajax:success', function() {
    window.location.reload();
  });

  $('#sign-in').on('ajax:error', function() {
    $('.message-error').html(I18n.t('invalid_email'));
  });
});
