class CreateBlog < ActiveRecord::Migration[5.0]
  def change
    create_table :blogs do |t|
      t.string :title
      t.integer :category, default: 0
      t.datetime :public_time
      t.boolean :is_public, default: true
      t.boolean :is_recommend, default: false
      t.string :image
      t.string :body
    end
  end
end
