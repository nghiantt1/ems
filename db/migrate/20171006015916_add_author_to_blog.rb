class AddAuthorToBlog < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :author_name, :string, default: Settings.author.name
    add_column :blogs, :author_position, :string, default: Settings.author.position
    add_column :blogs, :author_age, :integer, default: Settings.author.age
    add_column :blogs, :author_picture, :string, default: Settings.author.image
  end
end
