class ReactsController < ApplicationController
  before_action :load_react, only: [:update, :destroy]
  before_action :create_token

  def create
    return unless request.xhr?
    if params[:user_id].nil?
      create_with_token
    else
      create_params
    end
    @react.save
  end

  def update
    return unless request.xhr?
    if params[:user_id].nil?
      update_with_token
    else
      update_params
    end
  end

  def destroy
    @react.destroy if request.xhr?
  end

  private

  def create_with_token
    @react = React.new(react_params.merge!(code: cookies[:guest_token]))
  end

  def create_params
    @react = React.new react_params
  end

  def update_with_token
    @react.update_attributes(code: cookies[:guest_token], emotion: params[:emotion])
  end

  def update_params
    @react.update_attributes emotion: params[:emotion]
  end

  def react_params
    params.require(:react).permit React::ATTR_PARAMS
  end

  def load_react
    @react = if current_user.present?
               React.find_by_user_id_and_blog_id(params[:user_id],
                                                 params[:blog_id])
             else
               React.find_by_code_and_blog_id(cookies[:guest_token],
                                              params[:blog_id])
             end
  end

  def create_token
    cookies.permanent[:guest_token] ||= React.new_token
  end
end
