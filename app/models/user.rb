class User < ApplicationRecord
  has_many :reacts, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
         omniauth_providers: [:facebook, :google_oauth2]

  ATTR_PARAMS = [
    :name,
    :email
  ].freeze

  enum role: %w[user admin guess]

  class << self
    def find_for_google_oauth2(access_token, _signed_in_resource = nil)
      data = access_token.info
      user = User.where(provider: access_token.provider,
                        uid: access_token.uid).first
      unless user
        registered_user = User.where(email: access_token.info.email).first
        if registered_user
          return registered_user
        else
          user = User.create name: data['name'],
                             provider: access_token.provider,
                             email: data['email'],
                             uid: access_token.uid,
                             password: Devise.friendly_token[0, 20]
        end
      end
      user
    end

    def from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.email = auth.info.email
        user.password = Devise.friendly_token[0, 20]
        user.name = auth.info.name
      end
    end

    def new_with_session(params, session)
      super.tap do |user|
        if data == session['devise.facebook_data'] &&
           session['devise.facebook_data']['extra']['raw_info']
          user.email = data['email'] if user.email.blank?
        end
      end
    end
  end
end
