$(document).ready(function(){
  $('.preview-btn').on('click', function(){
    $('#modal-content').html(CKEDITOR.instances.blog_body.getData());
    $("#preview-content").modal();
  });

  $('.confirm-blog').on('click', function(){
    checkValue('.blog-title', '#title-error', I18n.t("errors.blank_title"));
    checkLength('.blog-title', 255, '#title-error', I18n.t("errors.length_title"));

    checkValue('.blog-author-name', '#author-name-error', I18n.t("errors.blank_name"));
    checkLength('.blog-author-name', 32, '#author-name-error',
      I18n.t("errors.length_name"));

    checkValue('.blog-author-position', '#author-position-error',
      I18n.t("errors.blank_position"));
    checkLength('.blog-author-position', 32, '#author-position-error',
      I18n.t("errors.length_position"));

    checkValue('#blog-time', '#public-time-error', I18n.t("errors.blank_public"));

    checkValue('.category-blog', '#category-error', I18n.t("errors.blank_category"));

    checkValueBody('#body-error', I18n.t("errors.body_blank"));

    checkSizeImage('blog_image', '#image-error', I18n.t("errors.image_size"));
    checkValueImage('.image-create', 'blog_image', '#image-error',
      I18n.t("errors.blank_image"));

    checkImageFormat('blog_image', '#image-error', I18n.t("errors.image_format"));
    checkImageFormat('blog_author_picture', '#author-picture-error',
      I18n.t("errors.image_format"));

    backToTop();

    if (checkValue('.blog-title', '#title-error', I18n.t("errors.blank_title")) ||
      checkLength('.blog-title', 255, '#title-error', I18n.t("errors.length_title")) ||
      checkValue('.blog-author-name', '#author-name-error', I18n.t("errors.blank_name"))||
      checkLength('.blog-author-name', 32, '#author-name-error', I18n.t("errors.length_name"))||
      checkValue('.blog-author-position', '#author-position-error', I18n.t("errors.blank_position")) ||
      checkLength('.blog-author-position', 32, '#author-position-error', I18n.t("errors.length_position")) ||
      checkValue('#blog-time', '#public-time-error', I18n.t("errors.blank_public")) ||
      checkValue('.category-blog', '#category-error', I18n.t("errors.blank_category")) ||
      checkSizeImage('blog_image', '#image-error', I18n.t("errors.image_size")) ||
      checkValueImage('.image-create', 'blog_image', '#image-error', I18n.t("errors.blank_image")) ||
      checkImageFormat('blog_image', '#image-error', I18n.t("errors.image_format")) ||
      checkImageFormat('blog_author_picture', '#author-picture-error', I18n.t("errors.image_format"))) {
      return false;
    }

    getValue();
  });

  $('.cancel-confirm').on('click', function(){
    showInput('.blog-title', '#title-confirm');
    showInput('.category-blog', '#category-confirm');
    showInput('#blog-time', '#public-time-confirm');
    showInput('.blog-author-age', '#author-age-confirm');
    showInput('.blog-author-name', '#author-name-confirm');
    showInput('.blog-author-position', '#author-position-confirm');

    $('.confirm-list-button').removeClass('hidden-input');
    $('#save-button').addClass('hidden-input');

    $('.input-image').removeClass('hidden-input');

    showvalueBody('.body-input', '.body-confirm');
    showValue('#blog_is_public', '.is-public-confirm');
    showValue('#blog_is_recommend', '.is-recommend-confirm');

    backToTop();
  });

  function showValue(dom_input, dom_show){
    $(dom_input).removeClass('hidden-input');
    $(dom_show).html('');
  }

  function showvalueBody(dom_input, dom_show){
    $(dom_input).removeClass('hidden-input');
    $(dom_show).html('');
    $(dom_show).addClass('hidden-input');
  }

  var checkValue = function(input, error, message){
    var value = $(input).val();
    if (value === "") {
      $(error).html(message);
      return true;
    }
  };

  function checkValueBody(error, message) {
    if (CKEDITOR.instances.blog_body.getData() === ""){
      $(error).html(message);
      return true;
    } else {
      $(error).html('');
    }
  }

  var checkLength = function(input, max_length, error, message){
    var string_length = $(input).val().length;
    if (string_length > max_length) {
      $(error).html(message);
      return true;
    }
  }

  function checkValueImage(dom_create, dom_input, error, message_require){
    var input = document.getElementById(dom_input);
    if(($(dom_create).length > 0) && (input.files.length === 0)){
      $(error).html(message_require);
      return true;
    }
  }

  function checkSizeImage(dom_input, error, message_over){
    var input = document.getElementById(dom_input);
    if ((input.files.length > 0) && ((input.files[0].size / 1024000) > 2)) {
      $(error).html(message_over);
      return true;
    }
  }

  function checkImageFormat(dom_input, error, message){
    var input = document.getElementById(dom_input);
    var allowType = /(\.jpg|\.png)$/i;
    if(input.files.length > 0){
      if (!allowType.exec(input.files[0].name)){
        $(error).html(message);
        return true;
      }
    }
  }

  function getValue(){
    $('#title-confirm').html($('.blog-title').val());
    $('#category-confirm').html($('.category-blog').val());
    $('#public-time-confirm').html($('#blog-time').val());
    $('#author-name-confirm').html($('.blog-author-name').val());
    $('#author-position-confirm').html($('.blog-author-position').val());
    $('#author-age-confirm').html($('.blog-author-age').val());

    checkboxConfirm($('#blog_is_public'), 'blog_is_public', $('.is-public-confirm'), 'public', 'unpublic');
    checkboxConfirm($('#blog_is_recommend'), 'blog_is_recommend', $('.is-recommend-confirm'), 'recommend', 'unrecommend');

    hideInput('.blog-title');
    hideInput('.category-blog');
    hideInput('#blog-time');
    hideInput('.blog-author-name');
    hideInput('.blog-author-position');
    hideInput('.blog-author-age');

    $('.confirm-list-button').addClass('hidden-input');
    $('#save-button').removeClass('hidden-input');

    $('.input-image').addClass('hidden-input');

    $('.body-confirm').html(CKEDITOR.instances.blog_body.getData());
    $('.body-confirm').removeClass('hidden-input');
    $('.body-input').addClass('hidden-input');

    $('.save-blog').on('click', function(){
      $('.save-after-confirm').click();
    });
  }

  function hideInput(dom){
    $(dom).addClass('hidden-input');
    $(dom).removeClass('form-control');
  }

  function showInput(dom_input, dom_show){
    $(dom_input).removeClass('hidden-input');
    $(dom_input).addClass('form-control');
    $(dom_show).html('');
  }

  function checkboxConfirm(checkbox_tag, id, confirm, text1, text2){
    $(checkbox_tag).addClass('hidden-input');

    var checkbox = document.getElementById(id)
    if (checkbox.checked === true){
      $(confirm).html(text1);
    } else {
      $(confirm).html(text2)
    }
  }

  function backToTop(){
    // $('body,html').animate({
    //   scrollTop: 0
    // }, 800);
    $('html,body').scrollTop(0);
  }

  $('.blog-title').on('change', function(){
    checkValueOnChange('.blog-title', '#title-error', I18n.t("errors.blank_title"));
  });

  $('.category-blog').on('change', function(){
    checkValueOnChange('.category-blog', '#category-error', I18n.t("errors.blank_category"));
  });

  $('#blog-time').on('change', function(){
    checkValueOnChange('#blog-time', '#public-time-error', I18n.t("errors.blank_public"));
  });

  $('.blog-author-name').on('change', function(){
    checkValueOnChange('.blog-author-name', '#author-name-error', I18n.t("errors.blank_name"));
  });

  $('.blog-author-position').on('change', function(){
    checkValueOnChange('.blog-author-position', '#author-position-error',
      I18n.t("errors.blank_position"));
  });

  $('.upload-image').on('change', function(){
    $('#image-error').html('');
  })

  $('.upload-picture').on('change', function(){
    $('#author-picture-error').html('');
  });

  // $('.upload-image').on('change', function(){
  //   checkValueOnChange('.upload-image', '#image-error', I18n.t("errors.blank_image"));
  // });

  // $('.upload-picture').on('change', function(){
  //   checkValueOnChange('.upload-picture', '#author-picture-error',
  //     I18n.t("errors.blank_image"));
  // });

  function checkValueOnChange(dom_input, error, message){
    var value = $(dom_input).val();
    $(error).html('');
    if (value === '') {
      $(error).html(message);
    }
  }
  
  $(document).on('turbolinks:load', function(){
    $('body').on('click', '.setting-public', function(){
      var id = $(this).attr('value');
      $.ajax({
        url: '/admin/blogs/' + id,
        method: 'PATCH',
        data: {is_public: false},
        success: function(){
          $('#blog-' + id).remove();
          $('#blog' + id).html(I18n.t("admin.blogs.tbody_list_blog.unpublic"));
        },
        error: function() {
          swal(
            I18n.t('blog.oops'),
            I18n.t('blog.sth_wrong'),
            'error'
          );
        }
      });
    });

    $('body').on('click', '.setting-time', function(){
      var status;
      var id = $(this).attr('id');
      $('#change-time' + id).removeClass('hidden-input');
      $('.public-time' + id).addClass('hidden-input');

        $.ajax({
          type: "PATCH",
          url: "/admin/blogs/" + id,
          dataType: "json",
          success: function(data){
            status = data;
          }
        });

      $('#setting-time' + id).datetimepicker({
        step: 15,
        onClose: function () {
          $.ajax({
            url: '/admin/blogs/' + id,
            method: 'PATCH',
            data: {public_time: $('#setting-time' + id).val()},
            success: function(){
              var time_input = $('#setting-time' + id).val();
              var now = moment().format(I18n.t('time.formats.format_datetime_js'));
              var time = moment(time_input).format(I18n.t('time.formats.format_datetime_js'));
              $('#change-time' + id).addClass('hidden-input');
              $('.public-time' + id).html(moment(time).format(I18n.t('time.formats.format_datetime_ja')));
              $('.public-time' + id).removeClass('hidden-input');

              if (time > now) {
                $('#blog' + id).html(I18n.t("admin.blogs.tbody_list_blog.unpublic"));
                $('#blog-' + id).addClass('hidden-input');
                $('#blog-' + id).removeClass('btn');
              } else if (time <= now && status) {
                $('#blog' + id).html(I18n.t("admin.blogs.tbody_list_blog.public"));
                $('#blog-' + id).removeClass('hidden-input');
                $('#blog-' + id).addClass('btn');
              }
            },
            error: function(){
              swal(
                I18n.t('blog.oops'),
                I18n.t('blog.sth_wrong'),
                'error'
              );
            }
          });
        }
      });
    });
  })


  $('#sort-blog').on('change', function(){
    var sort_type = $(this).val();
    var tbody = $('.list-blogs');
    $.ajax({
      url: '/admin/blogs',
      dataType: 'html',
      method: 'GET',
      data: {sort: sort_type},
      success: function(data){
        tbody.html(data);
      },
      error: function(){
        swal(
          I18n.t('blog.oops'),
          I18n.t('blog.sth_wrong'),
          'error'
        );
      }
    });
  });
});
