class AddCodeToReact < ActiveRecord::Migration[5.0]
  def change
    add_column :reacts, :code, :string
  end
end
