class AddTimestampsToBlog < ActiveRecord::Migration[5.0]
  def change
    change_column :blogs, :is_public, :boolean, default: false
    change_table(:blogs) { |t| t.timestamps }
  end
end
