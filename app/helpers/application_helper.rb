module ApplicationHelper
  def devise_mapping
    Devise.mappings[:user]
    Devise.mappings[:admin]
  end

  def resource_name
    devise_mapping.name
  end

  def resource_class
    devise_mapping.to
  end

  def format_time(time, format)
    I18n.l time, format: format if time
  end

  def active_class(link_path)
    current_page?(link_path) ? 'actived' : ''
  end

  def present_modal
    if session[:present].nil?
      session[:present] = true
      return true
    end
    false
  end
end
