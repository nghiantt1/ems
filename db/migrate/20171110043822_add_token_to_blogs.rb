class AddTokenToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :token, :string
  end
end
