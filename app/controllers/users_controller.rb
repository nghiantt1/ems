class UsersController < ApplicationController
  before_action :load_user, only: [:update, :show, :edit]

  def show
    @user = User.find_by id: params[:id]
  end

  def edit; end

  def update
    @user.update_attributes user_params
    if @user.update_attributes user_params
      redirect_to user_path(@user)
    else
      render :edit
    end
  end

  private

  def load_user
    @user = User.find_by id: params[:id]
  end

  def user_params
    params.require(:user).permit User::ATTR_PARAMS
  end
end
