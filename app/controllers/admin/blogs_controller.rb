class Admin::BlogsController < ApplicationController
  before_action :authenticate_admin!
  before_action :load_blog, only: [:update, :edit]
  before_action :load_blogs, only: :index

  def index
    respond_to do |format|
      if request.xhr?
        sort_blog
        format.html do
          render partial: 'sort_blog',
                 locals: { sort_blogs: @sort_blogs }
        end
      end
      format.html
    end
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new create_blog_params
    default_author_picture if @blog.author_picture.blank?
    if @blog.save
      flash[:success] = t '.saved'
      redirect_to admin_blogs_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if request.xhr?
      render json: @blog.is_public
      update_params
    else
      update_blog
    end
  end

  def home; end

  private

  def load_blogs
    @search = Blog.ransack params[:q]
    @blogs = @search.result.order(created_at: :desc).page(params[:page])
                    .per Settings.list_blog
  end

  def sort_blog
    params[:sort] ||= :DESC
    @sort_blogs = Blog.sort_blog(params[:sort]).page(params[:page])
                      .per Settings.list_blog
  end

  def update_params
    if params[:public_time].present?
      @blog.update_attributes public_time: params[:public_time]
    elsif params[:is_public].present?
      @blog.update_attributes is_public: params[:is_public]
    end
  end

  def blog_params
    params.require(:blog).permit Blog::ATTR_PARAMS
  end

  def create_blog_params
    blog_params.merge! token: Blog.new_token
  end

  def update_blog
    if @blog.update_attributes blog_params
      flash[:success] = t '.updated'
      redirect_to admin_blogs_path
    else
      render :edit
    end
  end

  def load_blog
    @blog = Blog.find_by id: params[:id]
  end

  def default_author_picture
    path = File.join(Rails.root, 'app/assets/images/', Settings.author.image)
    File.open(path) do |f|
      @blog.author_picture = f
    end
  end
end
