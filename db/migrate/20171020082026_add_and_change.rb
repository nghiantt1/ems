class AddAndChange < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :is_admin, :boolean
    add_column :users, :role, :integer, default: 0
    change_column :blogs, :category, :integer, default: nil
    change_column :blogs, :author_picture, :string, default: nil
    change_column :blogs, :body, :text
  end
end
