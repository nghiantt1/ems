$(document).ready(function(){
  $('.menu-index').mousewheel(function(event, delta) {
    this.scrollLeft -= delta * 30;
    event.preventDefault();
  });

  $(window).on('scroll', function(){
    more_blogs_url = $('.pagination .next_page a').attr('href');
    if(more_blogs_url && ($(window).scrollTop() + $(window).height() >= ($(document).height() - 1))){
      $.getScript(more_blogs_url);
    }
  });

  $('.author-details-button').on('click', function(){
    var id = $('.author-details-button').attr('id');
    $.ajax({
      url: '/blogs/' + id,
      method: 'GET',
      dataType: 'html',
      success: function(data){
        $('.show-blog-form').remove();
        $('.show-author').html(data);
        $('html,body').scrollTop(0);
      },
      error: function(){
        swal(
          I18n.t('blog.oops'),
          I18n.t('blog.sth_wrong'),
          'error'
        );
      }
    });
  });

  $('body').on('click', '.back-button', function(){
    window.location.reload();
    $('html,body').scrollTop(0);
  });

  $('.profile').on('click', function(){
    $('.text-user').addClass('hidden-input');
    $('.name-user').addClass('form-control');
    $('.email-user').addClass('form-control');
    var name = $('.name-user').val();
    var email = $('.email-user').val();
    $.ajax({
      url: '/blogs/' + id,
      method: 'PATCH',
      data: { user: {name: name, email: email } },
      success: function(){

      },
      error: function(){
        swal(
          I18n.t('blog.oops'),
          I18n.t('blog.sth_wrong'),
          'error'
        );
      }
    });
  });

  $('#tab-recommend').on('click', function(){
    $('html,body').scrollTop(0);
  });

  $('.category-tab').on('click', function(){
    var category = $(this).attr('value');
    $.ajax({
      url: '/blogs',
      method: 'GET',
      dataType: 'json',
      data: { category_type: category },
      success: function(data){
        $('#tab' + category).html(data.category_blogs);
        $('html,body').scrollTop(0);
      },
      error: function(){
        swal(
          I18n.t('blog.oops'),
          I18n.t('blog.sth_wrong'),
          'error'
        );
      }
    });
  });

  $('#tab-recommend').on('click', function(){
    $.ajax({
      url: '/blogs',
      method: 'GET',
      dataType: 'json',
      success: function(data){
        $('.user-blog-recommend').html(data.recommend_blogs);
        $('html,body').scrollTop(0);
      }
    });
  });

  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
    localStorage.setItem('idActiveTab', $(e.target).attr('value'));
    var activeTab = localStorage.getItem('activeTab');
    var left = $(document).outerWidth() - $(window).width();
    if(activeTab === "#tab2") {
      $('#my-div').scrollLeft(1000);
    }
  });

  var activeTab = localStorage.getItem('activeTab');
  var idActiveTab = localStorage.getItem('idActiveTab');
  if(activeTab){
    $('#tab-' + idActiveTab).click();
    $('#menu-list a[href="' + activeTab + '"]').tab('show');
  }

  Vote.voted($('.no-love'), $('.loved'), $('.liked'), $('.no-like'),
    $('.sad'), $('.no-sad'), $('.sorrow'), $('.no-sorrow'));
  Vote.removeVoted($('.loved'), $('.no-love'));

  Vote.voted($('.no-like'), $('.liked'), $('.loved'), $('.no-love'),
    $('.sad'), $('.no-sad'), $('.sorrow'), $('.no-sorrow'));
  Vote.removeVoted($('.liked'), $('.no-like'));

  Vote.voted($('.no-sad'), $('.sad'), $('.liked'), $('.no-like'),
    $('.loved'), $('.no-love'), $('.sorrow'), $('.no-sorrow'));
  Vote.removeVoted($('.sad'), $('.no-sad'));

  Vote.voted($('.no-sorrow'), $('.sorrow'), $('.loved'), $('.no-love'),
    $('.liked'), $('.no-like'), $('.sad'), $('.no-sad'));
  Vote.removeVoted($('.sorrow'), $('.no-sorrow'));
});

var Vote = {
  voted: function(dom_no_vote, dom_voted, vote1, no_vote1, vote2, no_vote2, vote3, no_vote3){
    var blog_id = $('.blog-id').val();
    var user_id = $('.user-id').val();
    var emotion = $(dom_voted).attr('value');

    $(dom_no_vote).on('click', function(){
      if (Vote.removeAnotherVoted($(vote1), $(no_vote1)) ||
        Vote.removeAnotherVoted($(vote2), $(no_vote2)) ||
        Vote.removeAnotherVoted($(vote3), $(no_vote3))) {
        var anotherVote = localStorage.getItem('anotherVote');

        $.ajax({
          url: '/reacts',
          method: 'PATCH',
          data: { user_id: user_id, blog_id: blog_id, emotion: emotion },
          success: function(){
            Vote.removeAllOtherVoted(dom_no_vote, dom_voted, vote1, no_vote1,
                vote2, no_vote2, vote3, no_vote3);
            $('.count-' + emotion).html(parseInt($('.count-' + emotion).html()) + 1);
            $('.count-' + anotherVote).html(parseInt($('.count-' + anotherVote).html()) - 1);
          },
          error: function(){
            swal(
              I18n.t('blog.oops'),
              I18n.t('blog.sth_wrong'),
              'error'
            );
          }
        });
      } else {
        $.ajax({
          url: '/reacts',
          method: 'POST',
          data: { react: { user_id: user_id, blog_id: blog_id, emotion: emotion } },
          success: function(){
            Vote.removeAllOtherVoted(dom_no_vote, dom_voted, vote1, no_vote1,
              vote2, no_vote2, vote3, no_vote3);
            $('.count-' + emotion).html(parseInt($('.count-' + emotion).html()) + 1);
          },
          error: function(){
            swal(
              I18n.t('blog.oops'),
              I18n.t('blog.sth_wrong'),
              'error'
            );
          }
        });
      }
    });
  },

  removeVoted: function(dom_voted, dom_no_vote){
    var blog_id = $('.blog-id').val();
    var user_id = $('.user-id').val();
    var emotion = $(dom_voted).attr('value');

    $(dom_voted).on('click', function(){
      $.ajax({
        url: '/reacts',
        method: 'DELETE',
        data: { blog_id: blog_id, user_id: user_id },
        success: function(){
          $(dom_voted).addClass('hidden-input');
          $(dom_no_vote).removeClass('hidden-input');
          $('.count-' + emotion).html(parseInt($('.count-' + emotion).html()) - 1);
        },
        error: function(){
          swal(
            I18n.t('blog.oops'),
            I18n.t('blog.sth_wrong'),
            'error'
          );
        }
      });
    });
  },

  removeAnotherVoted: function(another_voted, dom_no_vote){
    if ($(another_voted).hasClass('hidden-input') === false) {
      $(another_voted).addClass('hidden-input');
      $(dom_no_vote).removeClass('hidden-input');
      localStorage.setItem('anotherVote', $(another_voted).attr('value'));
      return true;
    }
  },

  removeAllOtherVoted: function(dom_no_vote, dom_voted, vote1, no_vote1, vote2,
    no_vote2, vote3, no_vote3){
    $(dom_no_vote).addClass('hidden-input');
    $(dom_voted).removeClass('hidden-input');
    Vote.removeAnotherVoted($(vote1), $(no_vote1));
    Vote.removeAnotherVoted($(vote2), $(no_vote2));
    Vote.removeAnotherVoted($(vote3), $(no_vote3));
  }
}
