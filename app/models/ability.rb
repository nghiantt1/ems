class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    elsif user.user?
      can :read, Blog
      can [:edit, :update], User, id: user.id
      can [:create, :destroy], React, user_id: user.id
    else
      can :read, Blog
    end
  end
end
