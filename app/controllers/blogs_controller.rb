class BlogsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :check_token, only: :show
  before_action :load_react, only: [:show, :index]

  def index
    @blogs = Blog.public_blogs.order(public_time: :desc).page(params[:page]).per 5
    @recommend_blogs = Blog.recommend_blogs.order(public_time: :desc).page(params[:page]).per 5
    @category_blogs = Blog.blog_by_category(params[:category_type])
                          .order(public_time: :desc).page(params[:page]).per 5
    respond_to do |format|
      format.html
      format.js
      return unless request.xhr?
      format.json { data_json }
    end
  end

  def show
    respond_to do |format|
      if request.xhr?
        format.html do
          render partial: 'author',
                 locals: { blog: @blog }
        end
      end
      format.html
    end
  end

  def app_author; end

  private

  def load_react
    @react = if current_user.present?
               React.find_by_user_id_and_blog_id(current_user.id, params[:id])
             else
               React.find_by_code_and_blog_id(cookies[:guest_token], params[:id])
             end
  end

  def data_json
    render json: {
      category_blogs: render_to_string(
        partial: 'blogs/category_blogs',
        locals: { category_blogs: @category_blogs },
        formats: 'html',
        layout: false
      ),
      recommend_blogs: render_to_string(
        partial: 'blogs/recommend_blogs',
        locals: { recommend_blogs: @recommend_blogs },
        formats: 'html',
        layout: false
      )
    }
  end

  def check_token
    if params[:token].present?
      @blog = Blog.find_by(token: params[:token], id: params[:id])
    else
      authenticate_user!
      @blog = Blog.find_by id: params[:id]
    end
  end
end
