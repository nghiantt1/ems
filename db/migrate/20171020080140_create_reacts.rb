class CreateReacts < ActiveRecord::Migration[5.0]
  def change
    create_table :reacts do |t|
      t.references :user, index: true, foreign_key: true
      t.references :blog, index: true, foreign_key: true
      t.string :content
      t.integer :emotion
      t.timestamps
    end
  end
end
